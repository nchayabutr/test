(function() {
    'use strict';
  
    window.addEventListener('load', function() {
      var form = document.getElementById('register-form');
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    }, false);
  })();


// $(function() {
//     // Initialize form validation on the registration form.
//     $( "#register-form" ).validate({
//       // validation rules
//       rules: {
//         firstname: "required",
//         lastname: "required",
//         username: {
//             required: true,
//             minlength: 2
//         },
//         password: {
//             required: true,
//             minlength: 8,
//             maxlength: 15
//         },
//         email: {
//             required: true,
//             email: true
//         },
//         state: "required"
//       },
//       // validation error messages
//       messages: {
//         firstname: "First Name is required",
//             lastname: "Last Name is required",
//             username: {
//                 required: "Username is required",
//                 minlength: "Your username must consist of at least 2 characters"
//             },
//             password: {
//                 required: "Please provide a password",
//                 minlength: "Your password must be at least 5 characters long"
//             },
            
//             email: "Email is not a valid address",
//             state: "State of Residency is required"
//       },
//     // submit to destination in "action" attribute of the form when valid
//     //  submitHandler: function(form) {
//     //    form.submit();
//     //  }
//     });
//   });